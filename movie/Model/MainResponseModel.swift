//
//  MainResponseModel.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/16/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import ObjectMapper

class MainResponse : NSObject, Mappable{
    
    var status : String?
    
    required init? (map:Map){}
    
    func mapping(map: Map) {
        status <- map["status"]
    }
    
}


