//
//  MovieModel.swift
//  movie
//
//  Created by Oscar Perdanakusuma Adipati on 29/06/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import ObjectMapper
import Foundation

class MovieModel : Mappable{
    
    var data: [movieData]?
    var page: String?
    var totalResults: String?
    var totalPages: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        data <- map["results"]
        page <- map["page"]
        totalResults <- map["total_results"]
        totalPages <- map["total_pages"]
    }
    
}

class movieData : NSObject, Mappable{
    
    var originalTitle: String?
    var releaseDate: String?
    var popularity: Double?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        originalTitle <- map["original_title"]
        releaseDate <- map["release_date"]
        popularity <- map["popularity"]
    }
    
}

