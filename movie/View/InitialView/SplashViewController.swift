//
//  SplashViewController.swift
//  movie
//
//  Created by Oscar Perdanakusuma Adipati on 29/06/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import AlamofireObjectMapper

class SplashViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            gotoHomePage()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func gotoHomePage(){
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.MOVIE_NAV)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

