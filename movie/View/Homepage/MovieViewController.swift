//
//  MovieViewController.swift
//  movie
//
//  Created by Oscar Perdanakusuma Adipati on 29/06/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import CoreData

class MovieViewController: BaseParentController {
    @IBOutlet weak var tableView: UITableView!
    var movieModel : MovieModel?
    var originalTitles = [String]()
    var releaseDates = [String]()
    var popularity = [Double]()
    var coreMovieList: [NSManagedObject] = []
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        setEnableTapGestureOnMainView(isEnable: false)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "movieView", bundle: nil), forCellReuseIdentifier: "movieCell")
        
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            compareDataCount()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func callAPIMovie(){
        originalTitles.removeAll()
        releaseDates.removeAll()
        
        let url = NSURL(string:Constant.getAPIMovie())
        print("MOVIES URL:\(String(describing: url!))")
        Alamofire.request(Constant.getAPIMovie(), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<MovieModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.movieModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let datas = self.movieModel?.data
                            if ((datas?.count)! > 0) {
                                for i in 0..<(datas?.count)!  {
                                    let originalTitle = datas![i].originalTitle
                                    let releaseDate = datas![i].releaseDate
                                    let popularity = datas![i].popularity
                                    let dates = Constant.convertDateStringtoAnotherFormat(dateString: releaseDate ?? "", fromFormat: "YYYY-mm-dd", toFormat: "dd MMM YYYY"); self.originalTitles.append(originalTitle ?? "")
                                    self.releaseDates.append(dates ?? "")
                                    self.popularity.append(popularity ?? 0.0)
                                }
                                DispatchQueue.main.async {
                                    self.lblOtherResult.isHidden = true
                                    self.showMainLayout(view: self.tableView)
                                    self.coreMovies()
                                }
                                print("originalTitles:\(self.originalTitles)")
                            } else {
                                self.setNoResult()
                            }
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed to get data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed to get data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func setNoResult(){
        lblOtherResult.isHidden = false
        hideMainLayout(view: self.tableView)
        setLabelOtherResult(label: lblOtherResult, text: "No data", parentView: self.view)
    }
    
    func bottomNotification(){
        let notificationAlert = UIAlertController(title: "", message: "Penyimpanan lokal telah diperbaharui", preferredStyle: UIAlertController.Style.actionSheet)

        let updatedAction = UIAlertAction(title: "Tampilkan", style: .destructive) { (action: UIAlertAction) in
            self.getCoreMovies()
        }
        notificationAlert.addAction(updatedAction)
        self.present(notificationAlert, animated: true, completion: nil)
    }
    
    func coreMovies() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestMovies = NSFetchRequest<NSManagedObject>(entityName: "Movie")
        let sortPopularity = NSSortDescriptor(key: "popularity", ascending: false)
        fetchRequestMovies.sortDescriptors = [sortPopularity]
        fetchRequestMovies.fetchLimit = 10
        do {
            let movies = try managedContext.fetch(fetchRequestMovies)
            print("movies.count1:\(movies.count)")
            if (movies.count == 0) {
                for i in 0..<(originalTitles.count)  {
                    let insertMovies = NSEntityDescription.insertNewObject(forEntityName: "Movie", into: managedContext)
                    insertMovies.setValue(originalTitles[i], forKey: "originalTitle")
                    insertMovies.setValue(releaseDates[i], forKey: "releaseDate")
                    insertMovies.setValue(popularity[i], forKey: "popularity")
                    do {
                        try managedContext.save()
                        coreMovieList.append(insertMovies)
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }
                CustomUserDefaults.shared.setCoreDataCount("\(coreMovieList.count)")
                print("movies.count2:\(coreMovieList.count)")
                self.bottomNotification()
                self.getCoreMovies()
            } else {
                let deleteRequestMovies = NSBatchDeleteRequest( fetchRequest: fetchRequestMovies as! NSFetchRequest<NSFetchRequestResult>)
                do {
                    coreMovieList.removeAll()
                    try managedContext.execute(deleteRequestMovies)
                    try managedContext.save()
                    for i in 0..<(originalTitles.count)  {
                        let insertMovies = NSEntityDescription.insertNewObject(forEntityName: "Movie", into: managedContext)
                        insertMovies.setValue(originalTitles[i], forKey: "originalTitle")
                        insertMovies.setValue(releaseDates[i], forKey: "releaseDate")
                        insertMovies.setValue(popularity[i], forKey: "popularity")
                        do {
                            try managedContext.save()
                            coreMovieList.append(insertMovies)
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                    CustomUserDefaults.shared.setCoreDataCount("\(coreMovieList.count)")
                    print("movies.count3:\(coreMovieList.count)")
                    self.bottomNotification()
                    self.getCoreMovies()
                } catch let error as NSError {
                    print("Could not delete. \(error), \(error.userInfo)")
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func compareDataCount() {
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        let coredataCount = "\(CustomUserDefaults.shared.getCoreDataCount())"
        print("coredataCount:\(coredataCount)")
        if (coredataCount != "0") {
            self.getCoreMovies()
        } else {
            timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }
    
    @objc func update() {
        print("60 seconds updated")
        self.callAPIMovie()
    }

    func getCoreMovies() {
        originalTitles.removeAll()
        releaseDates.removeAll()
        popularity.removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestMovies = NSFetchRequest<NSManagedObject>(entityName: "Movie")
        let sortPopularity = NSSortDescriptor(key: "popularity", ascending: false)
        fetchRequestMovies.sortDescriptors = [sortPopularity]
        fetchRequestMovies.fetchLimit = 10
        do {
            coreMovieList = try managedContext.fetch(fetchRequestMovies)
            CustomUserDefaults.shared.setCoreDataCount("\(coreMovieList.count)")
            print("movies.count4:\(coreMovieList.count)")
            if (coreMovieList.count > 0) {
                for movie in coreMovieList {
                    originalTitles.append(movie.value(forKey: "originalTitle") as? String ?? "")
                    releaseDates.append(movie.value(forKey: "releaseDate") as? String ?? "")
                    popularity.append(movie.value(forKey: "popularity") as? Double ?? 0.0)
                }
                self.tableView.reloadData()
            } else {
                self.callAPIMovie()
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

extension MovieViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return originalTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieCellView
        let row = indexPath.row
        cell.lblOriginalTitle.text = originalTitles[row]
        cell.lblReleaseDate.text = releaseDates[row]
        cell.lblOriginalTitle.textColor = .black
        cell.lblReleaseDate.textColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
}

