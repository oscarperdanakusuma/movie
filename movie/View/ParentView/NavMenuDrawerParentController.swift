//
//  NavMenuDrawerParent.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 9/21/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class NavMenuDrawerParentController : MainParentController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let icMenu = UIImage(named: "menuIcon")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: icMenu, style: UIBarButtonItem.Style.plain, target: self, action: #selector(openNavDrawer(_:)))
    }
    
    @objc func openNavDrawer(_ sender: AnyObject){
//        if let drawerController = parent as? NavDrawerController{
//            drawerController.setDrawerState(.opened, animated: true)
//            return
//        }
//
//        if let drawerController = navigationController?.parent as? NavDrawerController{
//            drawerController.setDrawerState(.opened, animated: true)
//        }
    }
    
}

