//
//  MainParentLoginRegister.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 8/24/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class InitialParentController: AlamofireBaseParentController, UINavigationControllerDelegate{
    
    let customNavBar = UINavigationBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //STANDARD NAVIGATION BAR
        //        let icBack = UIImage(named: "ic_navigate_previous_white")
        //        self.navigationController?.navigationBar.backIndicatorImage = icBack
        //        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = icBack
        //        let logo = UIImage(named: "home_credit_logo")
        //        let imageView = UIImageView(image:logo)
        //        self.navigationItem.titleView = imageView
        //        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        //CUSTOMIZE NAVIGATION BAR
        self.navigationItem.backBarButtonItem?.imageInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)
        navigationController?.navigationBar.isTranslucent = true
        
        //SET TRANSPARENT BACK BUTTON IMAGE
        //self.navigationController?.navigationBar.backIndicatorImage = UIImage()
        //self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: nil, action: nil)
        
        //navigationItem.setHidesBackButton(true, animated: false)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        
        var navBarHeight : CGFloat = 70
        let deviceName = UIDevice.current.modelName
        if deviceName == "iPhone X"{
            navBarHeight += UIApplication.shared.statusBarFrame.height - 10
            print("yPOINT: \(navBarHeight)")
        }
        
        
        customNavBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: navBarHeight)
        
        let color = UIColor(string: Constant.sharedIns.color_blue)
        
        customNavBar.barTintColor = color
        customNavBar.tintColor = color
        customNavBar.backgroundColor = color
        
        customNavBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        customNavBar.shadowImage = UIImage()
        
        navigationItem.title = ""
        
        //        let bgImage = UIImage(named: "bg_new_rebranding")
        //        let ivBG = UIImageView(image:bgImage)
        //
        //        ivBG.frame = CGRect(x: 0, y: 0, width: customNavBar.frame.width, height: customNavBar.frame.height)
        
        
        let icLogo = UIImage(named: "ic_logo_home_credit_new")
        let ivLogo = UIImageView(image:icLogo)
        
        let xPos = (self.view.frame.width / 2) - (ivLogo.frame.width * 1.5 / 2)
        
        let yPos = (customNavBar.frame.height / 2) - (ivLogo.frame.height * 1.5 / 2) + (UIApplication.shared.statusBarFrame.height * 0.3 )
        
        ivLogo.frame = CGRect(x: xPos, y: yPos, width: ivLogo.frame.width * 1.5, height: ivLogo.frame.height * 2.0)
        
        //        ivLogo.frame = CGRect(origin: .zero, size: CGSize(width: ivLogo.frame.width, height: ivLogo.frame.height))
        //        ivLogo.center = customNavBar.center
        
        //customNavBar.addSubview(ivBG)
        customNavBar.addSubview(ivLogo)
        self.view.addSubview(customNavBar)
        
        showHideStatusBar(isShow: true)
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        CommonUtils.doneButtonAction()
    }
    
    func gotoHomePage(){
        AppController.sharedInstance.setClearGlobalObject()
//        let storyboard = UIStoryboard(name: Constant.sharedIns.HOMEPAGE_STORYBOARD, bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.HOMEPAGE_NAV)
//        self.present(vc, animated: true, completion: nil)
    }
}

