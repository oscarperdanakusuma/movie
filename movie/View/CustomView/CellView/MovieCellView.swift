//
//  MovieCellView.swift
//  movie
//
//  Created by Oscar Perdanakusuma Adipati on 29/06/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit

class MovieCellView: UITableViewCell {
    
    @IBOutlet weak var lblOriginalTitle: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var movieView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
