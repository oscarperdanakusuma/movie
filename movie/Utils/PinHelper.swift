//
//  PinHelper.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 03/10/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import Foundation
import BCryptSwift


class PinHelper{
    static let shared = PinHelper()
    
    let bcryptFormat : String = "$2a$04$"
    let subCharacterCount  : Int = 10
    
    func genHashedPIN(plainPIN : String) -> String{
        let pinHashed = BCryptSwift.hashPassword(plainPIN, withSalt: BCryptSwift.generateSaltWithNumberOfRounds(4))
        
        print("HASHED: \(String(describing: pinHashed!))")
        
        return randomHashed(hashed: pinHashed!)
    }
    
    private func randomHashed(hashed: String)->String{
        var uuID = UUID().uuidString
        
        let indexUUID = uuID.index(uuID.endIndex, offsetBy: -(subCharacterCount))
        uuID = uuID.substring(from: indexUUID)
        
        var withoutFormat = hashed.replacingOccurrences(of: bcryptFormat, with: "")
        let startIndex = withoutFormat.index(withoutFormat.startIndex, offsetBy: subCharacterCount)
        let first10Hashed = withoutFormat.substring(to: startIndex)
    withoutFormat.removeSubrange(withoutFormat.startIndex..<withoutFormat.index(withoutFormat.startIndex, offsetBy: subCharacterCount))
        
        return "\(first10Hashed)\(uuID)\(withoutFormat)"
    }
    
    func reverseRandomHashed(hashed : String) -> String{
        let firstIndex = hashed.index(hashed.startIndex, offsetBy: subCharacterCount)
        let first10Hashed = hashed.substring(to: firstIndex)
        
        var lastHashed : String = hashed
        lastHashed.removeSubrange(lastHashed.startIndex..<lastHashed.index(lastHashed.startIndex, offsetBy: subCharacterCount*2))
        
        return "\(bcryptFormat)\(first10Hashed)\(lastHashed)"
    }
    
    
    func isCorrectPIN(plainPin: String)->Bool{
        var hashedPIN = CustomUserDefaults.shared.getPIN()
        
        if(hashedPIN.isEmpty){
            AppController.sharedInstance.logout()
            return false
        }
        
        hashedPIN = reverseRandomHashed(hashed: hashedPIN)
        print("HASHED_ORIGINAL: \(hashedPIN)")
        if(BCryptSwift.verifyPassword(plainPin, matchesHash: hashedPIN))!{
            return true
        }else{
            return false
        }
        
    }
    
    
}
