
import Foundation
import UIKit

class CheckBox: UIButton {
    // Images
    var checkedImage = UIImage(named: "ic_check_box")! as UIImage
    var uncheckedImage = UIImage(named: "ic_check_box_outline_blank")! as UIImage
    
    var icColor = UIColor(string: Constant.sharedIns.color_red_main)
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            checkedImage = checkedImage.tintImage(color: icColor)!
            
            uncheckedImage = uncheckedImage.tintImage(color: icColor)!
            
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
