//
//  Movie+CoreDataProperties.swift
//  
//
//  Created by Oscar Perdanakusuma Adipati on 30/06/20.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie")
    }

    @NSManaged public var originalTitle: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var popularity: Double

}
